% Curtin LaTeX Thesis Template/Format - developed by Tristan Reed, 2014 - 2017.
% Based on recommendations from the Curtin GRS website and Spatial Sciences admin. 
% This class is released under the CC-BY-SA Australia licence.
\NeedsTeXFormat{LaTeX2e}[2012/01/01]
\ProvidesClass{curtinthesis}[2017/04/19 v0.3 Provides Spatial@Curtin thesis format]

% 'Fixed' command - I can't imagine you'd need to change this at all. There is 
% no 'of Technology' as per changes to the Curtin University Act 1966 which 
% were made in 2016.
\newcommand{\theUniversity}{Curtin University}

% There is probably a better way to do this below, but I don't know it.
\newcommand{\degree}[1]{\newcommand{\getDegree}{#1}}
\newcommand{\organisation}[1]{\newcommand{\getOrganisation}{#1}}

% Extend the existing Report class and make some changes to it. Note the use of 
% various commands and packages for typographic formatting.
\LoadClass[12pt, a4paper]{report}
\RequirePackage[left=4cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}
\RequirePackage{anyfontsize}
\RequirePackage{titlesec}
\RequirePackage{titling}

% Set the line spacing to 1.3 - LaTeX can't seem to count! This is actually 1.5.
\linespread{1.3}

% Remove paragraph indent - I think this is a personal taste thing too.
\setlength{\parindent}{0cm}

% Set the font to a 'Georgia'-esque one. I do not think there are any font 
% requirements and this was chosen for my own taste. Don't be gaudy though!
\RequirePackage{palatino}

% Title Page - can't seem to find the example of this anymore but I remember it 
% being very specific. Having modelled it before, I am basing it off that.
\newcommand\thesistitlepage{
	\begin{titlepage}
	\begin{center}
	\vspace*{\fill}
	% Enrolling Area
	{\bf {\fontsize{10}{12} \selectfont \getOrganisation\\}} 
	\vspace{14em}
	% Title of Thesis
	{\bf {\fontsize{12}{14} \selectfont \thetitle\\}}
	\vspace{6em}
	% Author
	{\bf {\fontsize{11}{13} \selectfont \theauthor\\}}
	\vspace{18em}
	% Statement
	{\bf {\fontsize{8}{10} \selectfont This thesis is presented for the Degree of\\
	\getDegree\\ of\\ \theUniversity\\}}
	\vspace{4em}
	% Month and Year
	{\bf {\fontsize{10}{12} \selectfont \thedate}}
	\vspace*{\fill}
	\end{center}
	\end{titlepage}
}

% There also seems to be no required standard for chapter numbering, so again I
% have done what I like the look of. Again feel free to change to your preferred 
% type of header styling. This one seems to be de facto too.
\titleformat{\chapter}[hang]{\centering \fontsize{16}{18} \bfseries}
	{\thechapter}{20pt}{\MakeUppercase}
\titlespacing*{\chapter}{0pt}{0pt}{10pt}

% Same as above for section, subsection and subsubsection. If you have more, you'll
% need to add them yourself. No centering though this time!
\titleformat{\section}[hang]{\fontsize{16}{18} \bfseries}{\thesection}{20pt}{}
\titlespacing*{\section}{0pt}{10pt}{10pt}
\titleformat{\subsection}[hang]{\fontsize{16}{18} \bfseries}{\thesubsection}{20pt}{}
\titlespacing*{\subsection}{0pt}{10pt}{10pt}
\titleformat{\subsubsection}[hang]{\fontsize{16}{18} \bfseries}{\thesubsubsection}{20pt}{}
\titlespacing*{\subsubsection}{0pt}{10pt}{10pt}

	
% Set up the page numbering in the top right. Seems to be a de facto standard I have 
% seen in some places but not in others. Thanks, Stack Overflow.
\RequirePackage{fancyhdr}
\setlength{\headheight}{15pt}
\fancypagestyle{default}{
	\fancyhf{}
	\fancyhead[RO,R]{\thepage}
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0pt}
}

% This seems to be voodoo magic, but it works.
\fancypagestyle{plain}{\pagestyle{default}}
\AtBeginDocument{\pagestyle{default}}

% Set up the environment for the Preface section. All this really does is set using 
% Roman numerals for the numbering. There seems to be no required standard for page 
% numbering, so I have done it the way I like it. Feel free to change this for 
% your purposes and taste.
\newenvironment{prefacesection}{
	\setcounter{chapter}{0}
    \pagenumbering{roman}
    \let\existChapter\chapter
    \renewcommand{\chapter}[1]{
        	\addtocounter{chapter}{1}
    	\addcontentsline{toc}{chapter}{\MakeUppercase{##1}}
    	\existChapter*{##1}
    }
}{}

% Command to generate Table of Contents. Due to the unusual style of the template, 
% this 'hack' is required.
\newcommand{\tocsection}{
	\renewcommand{\contentsname}{Table of Contents}
    \addcontentsline{toc}{chapter}{\MakeUppercase{\contentsname}}
    \tableofcontents
    \addcontentsline{toc}{chapter}{\MakeUppercase{\listfigurename}}
    \listoffigures
    \addcontentsline{toc}{chapter}{\MakeUppercase{\listtablename}}
    \listoftables
}

% The List of Abbreviations is a weird one. It doesn't really fit elsewhere, so let's
% make an environment for it. This allows for any extra lists you have.
\newenvironment{listsection}{
    \let\existChapter\chapter
    \renewcommand{\chapter}[1]{
        	\addtocounter{chapter}{1}
    	\addcontentsline{toc}{chapter}{\MakeUppercase{##1}}
    	\existChapter*{##1}
    }
}{}

% Set up the environment for the Main section. Again, not too many changes except for 
% clearing the page and then moving to Arabic numerals. Again, change the numbering 
% style for your taste. Not sure if the Disclaimer page is really a thing, but eh.
\newenvironment{mainsection}{
    \cleardoublepage
    \setcounter{chapter}{0}
    \pagenumbering{arabic}
}{
	\clearpage
	\setlength{\parindent}{0in}
	\centering{\Large \bf Disclaimer}\\
	~\\ % Cheating, but eh.
	Every reasonable effort has been made to acknowledge the owners of copyright material. 
	I would be pleased to hear from any copyright owner who has been omitted or incorrectly 
	acknowledged.
}

% Set up the environment for the Appendix section. With thanks to
% http://stackoverflow.com/questions/2785260/hide-an-entry-from-toc-in-latex
% for some inspiration.
\newenvironment{appendixsection}{
    \setcounter{chapter}{0}
	\setlength{\parindent}{0in}
    \let\existChapter\chapter
    \renewcommand{\chapter}[1]{
        	\addtocounter{chapter}{1}
    	\addcontentsline{toc}{chapter}
    		{\MakeUppercase{Appendix \alph{chapter} \hspace*{1em} ##1}}
    	\clearpage
    	\vspace*{\fill}
    	{\centering \fontsize{16}{18} \bfseries \MakeUppercase{Appendix \alph{chapter} \hspace*{2em} ##1} \par}
    	\vspace*{\fill}
    	\clearpage
    }
}{}

% Declaration page - again, haven't seen a standard format but I have seen this one 
% in a few theses.
\newcommand\declarationpage{
	\clearpage
	\setlength{\parindent}{0in}
	\vspace*{\fill}
	{\Large \bf Declaration}\\
	\vspace{10em}\\
	To the best of my knowledge and belief this thesis contains no material previously 
	published by any other person except where due acknowledgement has been made.\\
	~\\
	This thesis contains no material which has been accepted for the award of any other 
	degree or diploma in any university.\\
	\vspace{20em}\\
	Signature: \line(1,0){200}\\
	~\\
	Date: \line(1,0){200}\\
	\vspace*{\fill}
	\clearpage
}

% Quote page - absolutely and definitely a freeform one. This is just the way I do it.
\newcommand\quotepage[2]{
	\clearpage
	\setlength{\parindent}{0in}
	\vspace*{\fill}
	{\it ``#1''}
	\begin{flushright}
	-- #2
	\end{flushright}
	\vspace*{\fill}
	\clearpage
}