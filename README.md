# Spatial Sciences Thesis Template

This repository contains a LaTeX version of the thesis template as used by Spatial Sciences at Curtin University. The design 
is based on both the recommendations of the Graduate Research School and the Department of Spatial Sciences.

As such, this is released under the CC-BY-SA Australia licence, unless incompatible with the work of the above organisations, 
which are licensed under their own terms.

I also disclaim any liability for using this awful class I wrote. I have neatened it up a fair bit, but I am sure there are still 
some issues (use the Issue Tracker if you find them).

## Repository Contents

* LaTeX `.cls` file for the template;
* Associated `.tex` boilerplate files;
* Example rendered `.pdf` of the thesis.


## Using the Class
The class does have some package dependencies. It is designed to use pdfLaTeX for rendering and BibTeX for references. The 
packages required are as follows:

* `anyfontsize` - was the easiest way to get precise font formatting and;
* `geometry` - for margins and;
* `fancyhdr` - for the interesting page number positioning and;
* `titlesec` - was used for some titling and;
* `titling` - used for other titling and;
* `palatino` - my font choice here as I think it looks nice, you can disable on line 32 of the `.cls`.

## Existing Issues

* Bibliography - untested.
* Figure and Image formatting - untested.
* Git repository - branches need to be organised.
* Font size of titles - slightly larger for readability (desired behaviour).

## Contact

If you have any queries or questions, don't hesitate to contact me:

Tristan Reed, tristan.reed@curtin.edu.au